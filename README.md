# bp-logix-login-redirector (PUBLIC)

Depending upon which IdP the user logged in via, redirect to a campus-specific
BP Logix login URL.

## What does this do

Intended to be deployed behind the Shibboleth SP such that the `eppn` header has
a value like `bucky@wisc.edu` or `freddy@uwrf.edu`.

Given a request with such a header, parses out which Wisconsin campus the user
is from, computes from this the BP Logix login campus code (it's `UW` for
Madison and the domain name without the `.edu` for everyone else), and puts that
code on a BP Logix login URL to which it redirects the user via a `302` and
`Location`.

Given a request without such a header, responses `403 FORBIDDEN` stating that
the `eppn` header is required.

## Why do this

BP Logix product had difficulty relying upon the Wisconsin Federation Discovery
Service (WAYF), and even without that difficulty naive integration with the
Wisconsin Federation WAYF wouldn't work because the default slate of IdPs are
not in all cases the correct IdPs for use with BP Logix. (That's a solved
problem in the Instructure Canvas context, so it's a solvable problem here too).

This microservice works around that difficulty.

Place this microservice behind suitable Wisconsin Federation login, e.g. in
`my.wisconsin.edu`. Wisconsin users log in to MyUW per normal and access this
microservice. Their `eppn` is known. This microservice uses that `eppn` to
determine the correct BP Logix login URL to redirect them to, which URL includes
an indication of campus affiliation. BP Logix then does whatever it does to log
users in. That might rely upon the same IdP the user already logged in with and
so might benefit from single sign-on, but presumably it's less convenient but
still viable when it relies on some other IdP or arrangement.

## Testing

Run the unit tests

```shell
$ lein test

lein test bp-logix-login-redirector.core-test

Ran 4 tests containing 8 assertions.
0 failures, 0 errors.
```


Or spin up the server and then test from command line by setting the `eppn`
header.

Run the microservice locally

```shell
$ lein ring server

...
2019-04-04 14:27:49.558:INFO:oejs.Server:main: Started @1747ms
Started server on port 3000
```

Make a request

```shell
$ curl -H "eppn: apetro@wisc.edu" -v localhost:3000/

*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 3000 (#0)
> GET / HTTP/1.1
> Host: localhost:3000
> User-Agent: curl/7.54.0
> Accept: */*
> eppn: apetro@wisc.edu
>
< HTTP/1.1 302 Found
< Date: Thu, 04 Apr 2019 19:36:57 GMT
< Content-Type: text/html;charset=utf-8
< Location: https://uwss.bplogix.net/login.aspx?campus=UW&bp_nextpage=http%3a%2f%2fuwss.bplogix.net%2fworkspace.aspx
< Content-Length: 0
< Server: Jetty(9.4.12.v20180830)
<
* Connection #0 to host localhost left intact
```

## Deploying

Generate a `.war` file.

```shell
$ lein ring uberwar

...

Created /Users/apetro/code/myuw_gitdoit/bp-logix-login-redirector/target/bp-logix-login-redirector.war
```

Then deploy the `.war` file using Maven.

### To deploy a SNAPSHOT

```shell
mvn deploy:deploy-file
  -DrepositoryId=SOME_ID \
  -Durl=https://artifacts.doit.wisc.edu/artifactory/myuw-public-snapshots \
  -Dfile=bp-logix-login-redirector.war \
  -DgroupId=edu.wisc.my.app \
  -DartifactId=bp-logix-login-redirector \
  -Dversion=VERSION \
  -DgeneratePom=true \
  -DgeneratePom.description="Redirector for Wisconsin BP Logix login."
```

SOME_ID should be whatever ID you have for `myuw-public-snapshots` in your
`~/.m2/settings.xml`; mine is `artifacts.doit-myuw-public-snapshots`.

```xml
<server>
     <id>artifacts.doit-myuw-public-snapshots</id>
     <username>apetro</username>
     <password>REDACTED</password>
</server>
```

VERSION should match that in `project.clj`, e.g. `0.1.0-SNAPSHOT`.

### To deploy a release

Swich `repositoryId`, `url`, and `version` above to the appropriate values.

+ `repositoryId`: as configured in your local Maven `settings.xml`;
  mine is `artifacts.doit-myuw-public-releases`
+ `url`: `https://artifacts.doit.wisc.edu/artifactory/myuw-public-releases`
+ `version`: the appropraite not-SNAPSHOT version number; e.g. `1.0.0`

There are other ways to deploy this (as an executable .jar, or even in its own
Docker container!), but this `.war` via artifact repository is the one most
plausible in the status quo MyUW service.

## License

Apache2.
Copyright © 2019 Regents of the University of Wisconsin
