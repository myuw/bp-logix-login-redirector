(ns bp-logix-login-redirector.core
  (:require
    [clojure.string :as string]
  )
)

(defn eppn-suffix-from-request
  "Given a request with the eppn header,
  return the campus suffix, e.g. wisc.edu.
  Returns nil if eppn header not present."
  [request]
  (let [eppn ((:headers request) "eppn")]
    (if eppn
      (last (string/split eppn #"@"))
    )
  )
)

(defn campus-code-from-eppn-suffix
  "Given an eppn suffix e.g. wisc.edu
  returns BP Logix campus code e.g. UW"
  [eppn-suffix]
  (if (= "wisc.edu" eppn-suffix)
    "UW"
    (string/upper-case
      (first (string/split eppn-suffix #"\."))))
)

(defn login-url-for-campus
  "Given a BP Logix campus code,
  return the BP Logix login URL for that code."
  [campus-code]
  (str
    "https://uwss.bplogix.net/login.aspx?campus="
    campus-code
    "&bp_nextpage=http%3a%2f%2fuwss.bplogix.net%2fworkspace.aspx")
)

(defn handler [request]
  "Handle web request,
  redirecting to appropriate BP Logix login URL."
  (let [eppn-suffix (eppn-suffix-from-request request)]
    (if eppn-suffix
      {
        :status 302 ;; REDIRECT to Location
        :headers {
          "Content-Type" "text/html; charset=UTF-8"
          "Location"
            (login-url-for-campus
              (campus-code-from-eppn-suffix eppn-suffix))
        }
      }
      {
        :status 403 ;; FORBIDDEN
        :headers {
          "Content-Type" "text/html; charset=UTF-8"
        }
        :body "Redirector requires eppn header."
      }
    )
  )
)
