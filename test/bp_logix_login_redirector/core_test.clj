(ns bp-logix-login-redirector.core-test
  (:require [clojure.test :refer :all]
            [bp-logix-login-redirector.core :refer :all]))

(deftest test-eppn-suffix-from-request
  (testing "eppn-suffix-from-request extracts wisc.edu"
    (is (=
      "wisc.edu"
      (eppn-suffix-from-request {:headers {"eppn" "bucky@wisc.edu"}})
    ))
  )
  (testing "eppn-suffix-from-request returns nil when eppn not present"
    (is (=
      nil
      (eppn-suffix-from-request {:headers {}})
    ))
  )
)
(deftest test-campus-code-from-eppn-suffix
  (testing "converts wisc.edu to UW"
    (is (=
      "UW"
      (campus-code-from-eppn-suffix "wisc.edu")
    ))
  )
  (testing "converts uwlax.edu to UWLAX"
    (is (=
      "UWLAX"
      (campus-code-from-eppn-suffix "uwlax.edu")
    ))
  )
)
(deftest test-login-url-for-campus
  (testing "login-url-for-campus uses campus code as request param"
    (is (=
      "https://uwss.bplogix.net/login.aspx?campus=UWFICTIONAL&bp_nextpage=http%3a%2f%2fuwss.bplogix.net%2fworkspace.aspx"
      (login-url-for-campus "UWFICTIONAL")
    ))
  )
)
(deftest test-handler
  (testing "handler responds status 302 (redirect) for Madison user"
    (is (=
      302
      (:status (handler {:headers {"eppn" "bucky@wisc.edu"}}))
    ))
  )
  (testing "handler redirects River Falls user to correct login URL"
    (is (=
      "https://uwss.bplogix.net/login.aspx?campus=UWRF&bp_nextpage=http%3a%2f%2fuwss.bplogix.net%2fworkspace.aspx"
      ((:headers (handler {:headers {"eppn" "freddy@uwrf.edu"}})) "Location")
    ))
  )
  (testing "handler presents error when no eppn"
    (is (=
      403
      (:status (handler {:headers {}}))
    ))
  )
)
