(defproject edu.wisc.my.app/bp-logix-login-redirector "0.0.1-SNAPSHOT"
  :description "Redirects Wisconsin user to identity-appropriate BP Logix login URL"
  :url "https://git.doit.wisc.edu/myuw/bp-logix-login-redirector"
  :scm {
    :name "git"
    :url "https://git.doit.wisc.edu/myuw/bp-logix-login-redirector"}
  :license {
    :name "Apache License"
    :url "http://www.apache.org/licenses/LICENSE-2.0"
    :distribution :repo
  }
  :javac-options ["-target" "1.8"]
  :dependencies [
    [org.clojure/clojure "1.8.0"]
    [ring/ring-core "1.7.1"]
    [ring/ring-jetty-adapter "1.7.1"]
  ]
  :plugins [
    [lein-ring "0.12.4"]
    [de.doctronic/lein-deploy-war "0.2.1"]
  ]
  :pedantic? :warn
  :ring {
    :handler bp-logix-login-redirector.core/handler
    :uberwar-name "bp-logix-login-redirector.war"
  }
  :deploy-repositories [
    ["snapshots" "https://artifacts.doit.wisc.edu/artifactory/myuw-public-snapshots"]
    ["releases" "https://artifacts.doit.wisc.edu/artifactory/myuw-public-releases"]]
)
